#!/bin/bash
# declare STRING variable
RED='\033[0;31m'
BLUE='\033[0;33m'
CYAN='\033[0;36m'
BLINKS='\033[5m'
RESET= '\033[0m'
NC='\033[0m;' # No Color
BOLD='\033[1m'
STRING="${CYAN}----------------Pulling into production....------------------${NC}"
#print variable on a screen
echo -e $STRING
echo -e "${CYAN}--------------------Build production version-------------------${NC}"
yarn build
echo -e "${CYAN}--------------------Add files to git---------------------------${NC}"
git add .
echo -e "---------------------Make a commit-----------------------------${NC}"
echo -e "${BLUE}Enter commit title:${RESET}"
read committitle
git commit -m "$committitle"
echo -e "${CYAN}-----------------------------Push to git-----------------------------${NC}"
git push origin master
echo -e "${BOLD}!!!!!!!SUCCESS! Pushed to production${RESET}!!!!!!!!!!!"
