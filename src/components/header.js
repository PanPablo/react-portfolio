import React, { Component } from 'react';
import Hamburger from "./component-items/hamburger";
import Logo from "../images/logoPS.png"

class Header extends Component {

    render() {
        const headerElements = [];
        const headerContainer =
            <header className="header">
                <div className="header__container">
                    <img className="logo" src={Logo} alt="Logo">
                    </img>
                    <nav>
                        <ul className="menu">
                            <li><a href="#">HOME</a></li>
                            <li><a href="#services">SERVICES</a></li>
                            <li><a href="#skils">SKILLS</a></li>
                            <li><a href="#portfolio">PORTFOLIO</a></li>
                            <li><a href="#contact">CONTACT</a></li>
                        </ul>
                    </nav>
                </div>
                <Hamburger/>
        </header>
        headerElements.push(headerContainer);
        return headerElements
    }
}

export default Header;