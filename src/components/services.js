import React, { Component } from 'react';
import ServicesItem from "./component-items/services-item";
import TrackVisibility from 'react-on-screen';
import Loader from "./component-items/loader";
import { CSSTransitionGroup } from 'react-transition-group'


class ServicesGraph extends Component {
    constructor (props){
        super(props);
        this.state = {
            posts: [],
            isLoaded: false,
        }
    }

    componentDidMount() {
        return fetch(`http://admin.pawelstruminski.com/?rest_route=/my_endpoint/v1/services`)
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    posts: responseJson,
                    isLoaded: true
                })
            })
            .catch((error) => {
                console.error(error);
            })
    }

    render() {
        const {isLoaded} = this.state;
        if (!isLoaded) {
            return <Loader/>;
        } else {
            const graph = [];
            const graphItem =  <CSSTransitionGroup
                transitionName="example"
                transitionAppear={true}
                transitionAppearTimeout={500}
                transitionEnter={false}
                transitionLeave={false}>
                <div className="services__graph">
                    {this.state.posts.map((item, index) => (
                        <ServicesItem url={item.acf.image}
                                      title={item.post_title}
                                      text={item.post_content}
                                      key={index}/>
                    ))}
                </div>
            </CSSTransitionGroup>;
            graph.push(graphItem);
            return graph
        }
    }
}


const ComponentToTrack = ({ isVisible }) => {
    const style = {
        opacity: isVisible ? '1' : '0',
        transition: '1s all',
    };
    return <div className="container" style={style}>
        <h1 className="services-title">Services I provide</h1>
        <div className="underline"></div>
        <p className="services-info">I working with both individuals and businesses from all over the globe to create awsome websites and apilcations</p>
        <ServicesGraph />
    </div>;
}


class Services extends Component {

    render() {
        const servicesElements = [];
        const services =
           <section className="services" id="services">
               <TrackVisibility once offset={550}>
                   <ComponentToTrack />
               </TrackVisibility>
           </section>
        servicesElements.push(services);
        return servicesElements
    }
}

export default Services;