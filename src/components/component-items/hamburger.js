import React, { Component } from 'react';

class Hamburger extends Component {
    constructor(props) {
        super(props);
        this.state = {
        addClass: false
        }
    }

    toggle() {
        this.setState({addClass: !this.state.addClass});
    }

    render() {
        const hamburgerElements = [];
        let hamburgerStyle = {backgroundColor: this.props.color};
        let hamburgerClass = ["hamburger"];
        let menuClass = ["mobile-menu"];
        if(this.state.addClass) {
            hamburgerClass.push('hamburger__open');
            menuClass.push("mobile-menu__open");
        }
        const hamburger =
            <div className={hamburgerClass.join(' ')} onClick={this.toggle.bind(this)} style={hamburgerStyle}>
                <div className="bar1"/>
                <div className="bar2"/>
                <div className="bar3"/>
            </div>;

        const mobileMenu = <div className={menuClass.join(' ')} onClick={this.toggle.bind(this)}>
            <div className="mobile-menu__container">
            <ul className="mobile-menu__elements">
                <li><a href="#">HOME</a></li>
                <li><a href="#services">SERVICES</a></li>
                <li><a href="#skils">SKILLS</a></li>
                <li><a href="#portfolio">PORTFOLIO</a></li>
                <li><a href="#contact">CONTACT</a></li>
            </ul>
            </div>
        </div>;
        hamburgerElements.push(hamburger, mobileMenu);
        return hamburgerElements
    }

}

export default Hamburger