import React, { Component } from 'react';

class SkilsItem extends Component {

    render() {
        const skilsItemElements = [];
        const skilsItems =
            <div className="skil-item">
                <img src={this.props.url} alt="" />
                <h4 className="skil-title">{this.props.title}</h4>
            </div>
        skilsItemElements.push(skilsItems);
        return skilsItemElements
    }
}

export default SkilsItem;