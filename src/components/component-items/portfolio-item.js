import React, { Component } from 'react';


class PortfolioItem extends Component {
    constructor(props) {
        super(props);
        this.state = {isToggleOn: true};
    }

    handleClick = (event, index) => {
        this.setState({
            isToggleOn: !this.state.isToggleOn
        });
    };

    render() {
        const open = {zIndex: "999", opacity: "1"};
        const close = {zIndex: "-10", opacity: "0"};
        const styl = this.state.isToggleOn ? close : open;
        const Modal =
            <div className="modal-window" style={styl}>
                <img className="modal-image" src={this.props.url} alt=""/>
                <div className="close-button" onClick={event => this.handleClick(event)}>
                    <div className="outer">
                        <div className="inner">
                            <label>Close</label>
                        </div>
                    </div>
                </div>
                <div className='project-info'>
                    <h1>{this.props.title}</h1>
                    <p dangerouslySetInnerHTML={{ __html: this.props.content }}/>
                    <p>TECH STACK: {this.props.stack}</p>
                    <a href={this.props.link} target="_blank" rel="noopener noreferrer"><span className="arrow">&#8594;</span></a>
                </div>
            </div>;
        const skilsItemElements = [];
        const skilsItems =
            <div className="portfolio-item" onClick={event => this.handleClick(event)}>
                <img src={this.props.url} alt="" />
                <h4 className="project-title">{this.props.title}</h4>
            </div>
        skilsItemElements.push(skilsItems, Modal);
        return skilsItemElements
    }
}

export default PortfolioItem;