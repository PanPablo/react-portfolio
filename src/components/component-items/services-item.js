import React, { Component } from 'react';

class ServicesItem extends Component {

    render() {
        const servicesItemElements = [];
        const serviceItems =
            <div className="service-item">
                <img src={this.props.url} alt="" />
                <div>
                    <h4 className="service-title">{this.props.title}</h4>
                    <p className="service-text" dangerouslySetInnerHTML={{ __html: this.props.text }}/>
                </div>
            </div>
        servicesItemElements.push(serviceItems);
        return servicesItemElements
    }
}

export default ServicesItem;