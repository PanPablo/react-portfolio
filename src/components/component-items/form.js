import React, { Component } from 'react';



class Form extends Component {
    constructor(props){
        super(props);
        this.state = {
            value: "",
            mailSent: false,
            error: null}
    }

    handleChange = (event) => {
        const nameEl = event.target.name;
        this.setState({
            [nameEl]: event.target.value
        })
    }

    handleSubmit = (event) => {
        event.preventDefault();
        console.log(this.state)
    }

    render() {
        return <form className='contact-form'>
            <input type="text"
                   name="name"
                   placeholder="Your Name"
                   value={this.state.name}
                   onChange={this.handleChange}/>
            <input type="email"
                   name="email"
                   placeholder="Your Email"
                   value={this.state.email}
                   onChange={this.handleChange}/>
            <textarea className="form-message" name="message" placeholder="Your Message" value={this.state.message} onChange={this.handleChange} />
            <input type="submit" onClick={event => this.handleSubmit(event)} value="SEND MESSAGE"/>
        </form>
    }
}

export default Form;