import React, { Component } from 'react';

class Button extends Component {

    render() {
        const buttonElements = [];
        let buttonStyle = {backgroundColor: this.props.color};
        const button =
           <button className="main__button" style={buttonStyle}>
               {this.props.text}
           </button>;
        buttonElements.push(button);
        return buttonElements
    }
}

export default Button;