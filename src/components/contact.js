import React, { Component } from 'react';
import TrackVisibility from 'react-on-screen';
import Form from "./component-items/form";

const ComponentToTrack = ({ isVisible }) => {
    const style = {
        opacity: isVisible ? '1' : '0',
        transition: '1s all'
    };
    return <div className="container" style={style}>
        <h1 className="services-title">Get in touch</h1>
        <div className="underline"></div>
        <p className="services-info">Paweł Strumiński, pstruminski@gmail.com tel:790215864</p>
        <Form/>
    </div>
}


class Contact extends Component {

    render() {
        const contactElements = [];
        const contactSection =
            <section className='contact' id="contact">
                <TrackVisibility once offset={500}>
                    <ComponentToTrack/>
                </TrackVisibility>
            </section>
        contactElements.push(contactSection);
        return contactElements
    }
}

export default Contact;