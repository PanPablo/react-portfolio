import React, { Component } from 'react';
import TrackVisibility from 'react-on-screen';
import SkillsItem from "./component-items/skils-item";
import Loader from "./component-items/loader";
import {CSSTransitionGroup} from "react-transition-group";

class SkillsGraph extends Component {
    constructor(props) {
        super(props);
        this.state = {
            posts: [],
            isLoaded: false,
        }
    }

    componentDidMount() {
        return fetch(`http://admin.pawelstruminski.com/?rest_route=/skills_endpoint/v1/skills`)
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    posts: responseJson,
                    isLoaded: true
                })
            })
            .catch((error) => {
                console.error(error);
            })
    }
    render() {
        const {isLoaded} = this.state;
        if (!isLoaded) {
            return <Loader/>;
        } else {
            const graph = [];
            const graphItem =  <CSSTransitionGroup
                transitionName="example"
                transitionAppear={true}
                transitionAppearTimeout={500}
                transitionEnter={false}
                transitionLeave={false}>
                <div className="services__graph">
                    {this.state.posts.map((item, index) => (
                        <SkillsItem url={item.acf.language_logo}
                                    title={item.post_title}
                                    text={item.post_content}
                                    key={index}/>
                    ))}
                </div>
            </CSSTransitionGroup>;
            graph.push(graphItem);
            return graph
        }
    }

}

const ComponentToTrack = ({ isVisible }) => {
    const style = {
        opacity: isVisible ? '1' : '0',
        transition: '1s all'
    };
    return <div className="container" style={style}>
        <h1 className="skils-title">I got skills!</h1>
        <div className="underline"></div>
        <p className="skils-info">My websites and apps are build with latest web-development technologies.</p>
        <SkillsGraph />
    </div>;
}
class Skils extends Component {

    render() {
        const skilsElements = [];
        const skilsSection =
           <section className="skils-section" id="skils">
               <TrackVisibility once offset={550}>
                   <ComponentToTrack />
               </TrackVisibility>
           </section>
        skilsElements.push(skilsSection);
        return skilsElements
    }
}

export default Skils;