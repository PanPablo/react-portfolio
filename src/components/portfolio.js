import React, { Component } from 'react';
import TrackVisibility from 'react-on-screen';
import PortfolioItem from "./component-items/portfolio-item";
import Loader from "./component-items/loader";
import {CSSTransitionGroup} from "react-transition-group";
import Button from "./component-items/button";


class PortfolioGraph extends Component {
    constructor(props) {
        super(props);
        this.state = {
            posts: [],
            isLoaded: false,
        }
    }

    componentDidMount() {
        return fetch(`http://admin.pawelstruminski.com/?rest_route=/portfolio_endpoint/v1/portfolio`)
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    posts: responseJson,
                    isLoaded: true
                })
            })
            .catch((error) => {
                console.error(error);
            })
    }

    render() {
        const {isLoaded} = this.state;
        if (!isLoaded) {
            return <Loader/>;
        } else {
            const graph = [];
            const graphItem =  <CSSTransitionGroup
                transitionName="example"
                transitionAppear={true}
                transitionAppearTimeout={500}
                transitionEnter={false}
                transitionLeave={false}>
                <div className="portfolio-graph">
                    {this.state.posts.map((item, index) => (
                        <PortfolioItem key={index} url={item.acf.project_image} title={item.post_title} content={item.post_content} stack={item.acf.tech_stack} link={item.acf.link}  />
                    ))}
                </div>
            </CSSTransitionGroup>;
            graph.push(graphItem);
            return graph
        }
    }
}


const ComponentToTrack = ({ isVisible }) => {
    const style = {
        opacity: isVisible ? '1' : '0',
        transition: '1s all'
    };
    return <div className="container container__portfolio" style={style}>
        <h1 className="portfolio-title">Portfolio</h1>
        <div className="underline"></div>
        <p className="portfolio-info">My recent projects</p>
        <PortfolioGraph/>
        <Button text="Show more projects" color="rgb(23, 194, 164)" />
    </div>;
};

class Portfolio extends Component {

    render() {
        const portfolioElements = [];
        const portfolioSection =
            <section className="portfolio-section" id="portfolio">
                <TrackVisibility once offset={950}>
                    <ComponentToTrack />
                </TrackVisibility>
            </section>
        portfolioElements.push(portfolioSection);
        return portfolioElements
    }
}

export default Portfolio;