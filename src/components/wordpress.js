import React, { Component } from 'react';


class Wordpress extends Component {
    constructor (props){
        super(props);
        this.state = {
            posts: []
        }
    }

    componentDidMount() {
        return fetch(`http://localhost/reactwp/index.php/wp-json/my_endpoint/v1/services`)
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    posts: responseJson,
                })
            })
            .catch((error) => {
                console.error(error);
            })
    }

    render() {
        return (
            <div>
                {this.state.posts.map((item, index) => (
                    <div>
                        <div class="row">
                            <div class="leftcolumn">
                                <div class="card">
                                    <div className= "title">
                                        <h1>{item.post_title}</h1>
                                        <img src={item.acf.image} alt=""/>
                                    </div>
                                    <div className= "content" dangerouslySetInnerHTML={{ __html: item.post_content }} />
                                </div>
                            </div>
                        </div>
                    </div>
                ))}
            </div>
        )
    }
}

export default Wordpress;