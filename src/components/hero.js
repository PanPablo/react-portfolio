import React, { Component } from 'react';
import Button from './component-items/button'
import TrackVisibility from 'react-on-screen';

const ComponentToTrack = ({ isVisible }) => {
    const style = {
        opacity: isVisible ? '1' : '0',
        transition: '1s all'
    };
    return <div style={style}>
        <h1 className="hero__title">Hi there! I'm Front-end Developer and I build <br />awsome websites and apps</h1>
        <a href="#contact"><Button text="Work with me!" color="rgb(235, 125, 75)" /></a>
    </div>
}


class Hero extends Component {

    render() {
        const heroElements = [];
        const heroSection =
            <div className='hero'>
                <TrackVisibility once>
                    <ComponentToTrack/>
                </TrackVisibility>
            </div>
        heroElements.push(heroSection);
        return heroElements
    }
}

export default Hero;