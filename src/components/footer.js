import React, { Component } from 'react';
import react from "../images/Reactmini.png";

class Footer extends Component {
    
    render() {
        const headerElements = [];
        const headerContainer =
            <footer className="footer">
                <div className="header__container">
                    <ul className="footer-links">
                        <li><a href="https://github.com/panpablo">GitHub</a></li>
                        <li><a href="https://gitlab.com/PanPablo">GitLab</a></li>
                        <li><a href="https://www.linkedin.com/in/pawel-struminski/">LinkedIn</a></li>
                        <li><a href="#">2019 Copyrights: Pawel Struminski</a></li>
                        <li><a href="https://reactjs.org/">Build with REACT</a><img src={react} alt="react logo"/></li>
                    </ul>
                </div>
            </footer>
        headerElements.push(headerContainer);
        return headerElements
    }
}

export default Footer;