import React, { Component } from 'react';
import Header from './components/header'
import Hero from './components/hero'
import './styles/App.css';
import Services from "./components/services";
import Skils from "./components/skils";
import Portfolio from "./components/portfolio";
import Contact from "./components/contact";
import Footer from "./components/footer";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header/>
        <Hero/>
        <Services/>
        <Skils/>
        <Portfolio/>
        <Contact/>
        <Footer/>
      </div>
    );
  }
}

export default App;
